#author dbaduini

class {'play': 
   version => "2.2.3",
   user    => "ec2-user"
}

class { java: }

$jdk_tgz="jdk-7u60-linux-x64.tar.gz"
$jdk_url="http://download.oracle.com/otn-pub/java/jdk/7u60-b19/${jdk_tgz}"
$jdk_dest_dir="/etc/puppet/modules/java/files/"

# Download JDK
notice("Downloading JDK ${jdk_tgz}")
exec { 'wget_jdk':
  command => "/usr/bin/wget --no-check-certificate --no-cookies --header 'Cookie: oraclelicense=accept-securebackup-cookie' ${jdk_url} -P ${jdk_dest_dir}",
  unless  => "/usr/bin/test -f ${jdk_dest_dir}/${jdk_tgz}",
  timeout => 0
}

#Install JDK
java::setup {'jdk-7u60-linux-x64':
  source => 'jdk-7u60-linux-x64.tar.gz',
  deploymentdir => '/usr/java/jdk-7u60-linux-x64',
  user => 'root',
  pathfile => '/home/ec2-user/.bash_profile',
  cachedir => "/tmp/java-setup-${name}",
  require => Exec["wget_jdk"]
}
